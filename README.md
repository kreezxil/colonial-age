# colonial-age
Minecraft: The Colonial Experience

https://www.curseforge.com/minecraft/modpacks/colonial-age

## Description
A pack designed for the exprienced player who is an avid follower and donator towards the Minecolonies project.

 

Players will enjoy the latest alpha builds of the mod and experience brand new exciting content such as the Stone Mason, Metal Caster, Glazier, Glass Blower, Sculptor, Wall Builder, Outpost Magistrate and that's just the names of the people that will be inhabiting structures that your builders will be building.

 

Questing and adventure are also included in the pack.
